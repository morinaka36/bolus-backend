name := "bolus-backend"

version := "0.1"

scalaVersion := "2.13.4"

// Only necessary for SNAPSHOT releases
resolvers += Resolver.sonatypeRepo("snapshots")

val zioVersion = "1.0.3"
val zioLoggerVersion = "0.4.0"
val slf4jVersion = "1.7.22"
val http4sVersion = "0.21.14"
val circeVersion = "0.13.0"
val declineVersion = "1.3.0"

libraryDependencies ++= Seq(
  "dev.zio" %% "zio" % zioVersion,
  "dev.zio" %% "zio-logging" % zioLoggerVersion,
  "dev.zio" %% "zio-logging-slf4j" % zioLoggerVersion,
  "dev.zio" %% "zio-interop-cats" % "2.2.0.1",

  "org.slf4j" % "slf4j-simple" % slf4jVersion,
  "org.slf4j" % "slf4j-api" % slf4jVersion,

  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,

  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-literal" % circeVersion,

  "com.github.pureconfig" %% "pureconfig" % "0.14.0",

  "com.monovore" %% "decline" % declineVersion,
  "com.monovore" %% "decline-effect" % declineVersion
)