package com.bolus.backend

import pureconfig._
import pureconfig.generic.auto._
import zio._
import scala.util.Try

package object config {
  case class Config(http: HttpConfig)
  case class HttpConfig(endpoint: String, port: Int)

  type ConfigService = Has[Service]

  trait Service {
    def load: Task[Config]
  }

  val live: ULayer[ConfigService] = ZLayer.succeed(new Service {
    override def load: Task[Config] = Task.fromTry { Try { ConfigSource.default.loadOrThrow[Config] } }
  })

  def get: RIO[ConfigService, Config] = ZIO.accessM[ConfigService](_.get.load)

  def http: RIO[ConfigService, HttpConfig] = get map { _.http }
}
