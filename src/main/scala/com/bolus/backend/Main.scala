package com.bolus.backend

import com.bolus.backend.cli.Cli
import zio.console.putStrLn
import zio._

object Main extends App {
  override def run(args: List[String]): URIO[ZEnv, ExitCode] = {
    val logic = ZIO.accessM[Cli](_.get.run(args))

    val httpLayer = ZEnv.live ++ config.live >>> http.live
    val consoleLayer = ZEnv.live ++ config.live ++ httpLayer >>> cli.live

    val layers = httpLayer ++ consoleLayer

    val program = logic
      .provideSomeLayer[ZEnv](layers)
      .foldM(
        err => putStrLn(s"Failed with cause $err") *> ZIO.succeed(ExitCode.failure),
        exitCode => ZIO.succeed(exitCode),
      )

    program
  }
}
