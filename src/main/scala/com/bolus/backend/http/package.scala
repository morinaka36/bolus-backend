package com.bolus.backend

import com.bolus.backend.config.ConfigService
import com.bolus.backend.http.controller._
import org.http4s._
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import zio._
import zio.interop.catz._
import zio.interop.catz.implicits._

package object http {
  type Http = Has[Service]

  trait Service {
    def serve: Task[ExitCode]
  }

  val live: URLayer[ConfigService with ZEnv, Http] = {
    def buildService(runtime: Runtime[ZEnv], configService: config.Service) = new Service {
      override def serve: Task[ExitCode] = ZIO.succeed(runtime) flatMap { implicit runtime =>
        for {
          config <- configService.load
          code   <- BlazeServerBuilder[Task](runtime.platform.executor.asEC)
            .bindHttp(config.http.port, config.http.endpoint)
            .withHttpApp(routes(configService).orNotFound)
            .resource
            .toManagedZIO
            .useForever
            .as(ExitCode.success)
        } yield code
      }
    }

    val z = for {
      runtime       <- ZIO.runtime[ZEnv]
      configService <- ZIO.access[ConfigService](_.get)
    } yield buildService(runtime, configService)

    z.toLayer
  }

  private def routes(configService: config.Service): HttpRoutes[Task] = {
    val dsl = Http4sDsl[Task]

    val controllers: List[Controller] = List(
      ApiController(dsl, configService)
    )

    Router[Task](controllers map { c => c.routePrefix -> c.routes }: _*)
  }
}
