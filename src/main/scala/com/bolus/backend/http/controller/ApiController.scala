package com.bolus.backend.http.controller

import com.bolus.backend.config
import org.http4s._
import zio._
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import io.circe.generic.auto._
import org.http4s.dsl.Http4sDsl
import zio.interop.catz._

case class ApiController(dsl: Http4sDsl[Task], configService: config.Service) extends Controller {
  override val routePrefix: String = "/api"

  def routes: HttpRoutes[Task] = {
    import dsl._

    HttpRoutes.of[Task] {
      case GET -> Root => for {
        config   <- configService.load
        response <- Ok(config)
      } yield response
    }
  }
}
