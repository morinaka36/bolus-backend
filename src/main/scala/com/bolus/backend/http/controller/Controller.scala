package com.bolus.backend.http.controller

import org.http4s.HttpRoutes
import zio.Task

trait Controller {
  val routePrefix: String
  def routes: HttpRoutes[Task]
}
