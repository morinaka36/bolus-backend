package com.bolus.backend.cli.modules

import cats.effect.{ExitCode => CatsExitCode, _}
import com.bolus.backend._
import com.bolus.backend.http.Http
import com.monovore.decline._
import com.monovore.decline.effect._
import zio._
import zio.console.putStrLnErr
import zio.interop.catz.taskConcurrentInstance

case class DeclineCli(configService: config.Service, httpService: http.Service, zConsole: console.Console.Service) extends cli.Service {

  case class HttpServer()

  override def run(args: List[String]): Task[ExitCode] =
    CommandIOApp.run(command, args)
      .catchAll(t => zConsole.putStrLnErr(t.getMessage).as(cats.effect.ExitCode.Error))
      .map(ec => zio.ExitCode(ec.code))

  private val command: Command[Task[cats.effect.ExitCode]] = Command("bolus", "Bolus Backend CLI") {
    val httpServerOpts: Opts[HttpServer] = Opts.subcommand("http", "Start HTTP server") {
      Opts.unit.map(_ => HttpServer())
    }

    val program = httpServerOpts map {
      case HttpServer() => httpService.serve.as(cats.effect.ExitCode.Success)
    }

    program
  }
}
