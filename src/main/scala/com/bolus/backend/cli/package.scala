package com.bolus.backend

import com.bolus.backend.cli.modules.DeclineCli
import com.bolus.backend.config.ConfigService
import com.bolus.backend.http.Http
import zio._
import zio.console.Console

package object cli {
  type Cli = Has[Service]

  trait Service {
    def run(args: List[String]): Task[ExitCode]
  }

  val live: RLayer[ConfigService with Http with Console, Cli] = ZLayer.fromServices[config.Service, http.Service, console.Console.Service, Service]((config, http, console) => DeclineCli(config, http, console))
}